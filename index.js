
const path = require('path');
const crypto = require('crypto');

const projectid = "projectid";
const sign_password = "sign_password";
const accepturl = "http://example.lt/order.io-paysera_accept";
const cancelurl = "http://example.lt/order.io-paysera_cancel";
const callbackurl = "http://example.lt/order.io-paysera_callback";

module.exports = class {
  constructor(cmbird, table) {
    this.table = table;

    let app = cmbird.app;

    app.post('/order.io', async function(req, res) {
      try {
        const body = JSON.parse(req.body.data);

        console.log("ORDER.IO REQUEST BODY", body);
        switch (body.command) {
          case 'new':
            const order_id = await table.insert({
              customer_id: body.customer_id,
              amount: body.period
            });

            let parameters = {
              projectid: projectid,
              orderid: order_id,
              accepturl: accepturl,
              cancelurl: cancelurl,
              callbackurl: callbackurl,
              sign_password: sign_password,
              lang: "LIT",
              amount: body.amount,
              currency: "EUR",
              p_firstname: body.firstname,
              p_lastname: body.lastname,
              p_email: body.email,
              version: 1.6
            }

            var data = "";
            var first_item = true;
            for (var p in parameters) {
              if (!first_item) {
                data += "&";
              } else {
                first_item = false;
              }
              data += p+"="+encodeURIComponent(parameters[p]);
            }


            data = new Buffer(data).toString('base64');
            data = data.replace("/", "_").replace("+", "-");

            var sign = crypto.createHash('md5').update(data+sign_password).digest('hex');

            console.log("PAYSERA DATA + SIGNITURE :", data, sign);

            var result = {
              data: data,
              sign: sign
            }

            res.send(JSON.stringify(result));
            break;
          case 'confirm':

            break;
          default:
        }
      } catch (e) {
        console.error(e.stack);
      }
    });

    app.get("/order.io-paysera_callback", async function(req, res) {
      var request = req.query;

      const ss1 = crypto.createHash('md5').update(
        request.data + sign_password
      ).digest('hex');

      if (data.ss1 == ss1) {
        var prettyfiedEncodedData = request.data.replace('_', '/').replace('-', '+');
        var decodedUrl = new Buffer(prettyfiedEncodedData, 'base64').toString('ascii');
        const order = querystring.parse(decodedUrl);

        console.log("!! ORDER !!", order);

        const db_order = (await table.select(["customer_id", "amount"], "id = $1", [order.orderid]))[0];

        cmbird.auth.table.update(
          {
            locked: false,
            sutartis: true,
            galioja_iki: Date.now() + 1000 * 60 * 60 * 24 * 31 * db_order.amount
          }, "id = $1", [db_order.customer_id]
        )
      //res.send("OK");
      }
    });
  }

  static async init(cmbird) {
    var table = await cmbird.aura.table('orders', {
      columns: {
        id: 'SERIAL',
        customer_id: 'UUID',
        amount: 'integer'
      }
    });
    return new module.exports(cmbird, table);
  }
}
